import React from 'react';
import Header from './components/Header';
import Action from './components/Action';
import Options from './components/Options';
import AddOption from './components/AddOption';
import SelectedOption from './components/SelectedOption';
import 'normalize.css';
import './styles/styles.scss';

class IndecisionApp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: props.title,
      subtitle: props.subtitle,
      options: props.options,
      isModalOpen: undefined
    }
  }

  handlePick = () => {
    const optionNumber = Math.floor(Math.random() * this.state.options.length);
    const choosenOption = this.state.options[optionNumber];
    this.setState(() => ({
      isModalOpen: choosenOption
    }))
  }

  handleAddOption = (option) => {
    if (!option) {
      return 'Enter valid value to add item'
    } else {
      this.setState((prevState) => ({
        options: prevState.options.concat([option])
      }))
    };
  }

  handleDeleteOptions = () => {
    this.setState(() => ({ options: [] }));
  }

  handleDelete = (optionToRemove) => {
    this.setState((prevState) => ({
      options: prevState.options.filter((option) => {
        return optionToRemove !== option;
      })
    }))
  }

  handleCloseOpenModal = () => {
    this.setState(() => ({
      isModalOpen: false
    }))
  }

  render() {
    return (
      <div>
        <Header title={this.state.title} subtitle={this.state.subtitle} />
        <div className="container">
          <Action showButton={this.state.options.length > 0} handlePick={this.handlePick} />
          <Options options={this.state.options} deleteOptions={this.handleDeleteOptions} handleDelete={this.handleDelete} />
          <AddOption addOption={this.handleAddOption} />
          <SelectedOption openModal={this.state.isModalOpen} closeModal={this.handleCloseOpenModal} />
        </div>
      </div>
    )
  }
}


IndecisionApp.defaultProps = {
  title: 'Indecision App',
  subtitle: 'Put your life in the hands of the computer',
  options: [],
}



export default IndecisionApp;





