import React from 'react';
import Option from './Option';

const Options = (props) => (
  <div className="options">
    <ol>
      {props.options.map((option) => (
        <Option
          key={option}
          option={option}
          deleteOption={props.handleDelete}
        />
      ))
      }
    </ol>
    <button className="options__button" onClick={props.deleteOptions}>remove all</button>
  </div>
)

export default Options;