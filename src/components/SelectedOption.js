import React from 'react';
import ReactModal from 'react-modal';

const SelectedOption = (props) => (
  <ReactModal
    isOpen={!!props.openModal}
    onRequestClose={props.closeModal}>

    <h3>Selected Option</h3>
    {props.openModal && <p>{props.openModal}</p>}
    
    <button onClick={props.closeModal}>okay</button>

  </ReactModal>
)

export default SelectedOption;