import React from 'react';

const Option = (props) => (
  <li className="option">
    {props.option}
    <button className="option__button" onClick={(e) => {
      props.deleteOption(props.option);
    }}>delete</button>
  </li>
)

export default Option;