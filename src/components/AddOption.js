import React from 'react';

class AddOption extends React.Component {
  constructor(props) {
    super(props);
  }

  handleOnSubmit = (e) => {

    e.preventDefault();
    const value = e.target.elements.option.value.trim();
    if (value) {
      this.props.addOption(value);
      e.target.elements.option.value = '';
    }
  }

  render() {
    return (
      <div className="addoption">
        <form className="addoption__form" onSubmit={this.handleOnSubmit}>
          <input className="addoption__input" type="text" name="option"/>
          <button className="addoption__button">Add</button>
        </form>
      </div>
    )
  }
}

export default AddOption;