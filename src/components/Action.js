import React from 'react';

const Action = (props) => (
  <div className="action">
    <button className="action__button" disabled={!props.showButton} onClick={props.handlePick}>What should I do ?</button>
  </div>
)

export default Action;